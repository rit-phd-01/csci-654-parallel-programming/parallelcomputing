#include "mpi.h"
#include<bits/stdc++.h>

using namespace std;

class Point {
private:
    double x;
private:
    double y;
public:
    Point() {
        this->x = 0;
        this->y = 0;
    }

public:
    Point(double x, double y) {
        this->x = x;
        this->y = y;
    }

public:
    double getX() {
        return this->x;
    }

public:
    double getY() {
        return this->y;
    }
};

vector<Point *> generate_random_points(int n, int side, double seed, double minVal, double maxVal) {
    // Generate a vector of points
    vector < Point * > p;
    // rd() provides a random seed
    random_device rd;
    default_random_engine generator(rd());
    uniform_real_distribution<double> distribution(minVal, maxVal);
    for (int i = 0; i < n; i++) {
        double x = distribution(generator);
        double y = distribution(generator);
        Point *temp = new Point(x, y);
        p.push_back(temp);
    }
    return p;
}

double *find_largest(int n, int side, int seed, int minVal, int maxVal, int startIndex, int endIndex, int rank) {
    int i, j, k;
    static double largestIndexes[4];
    double ax, ay, bx, by, cx, cy, largestArea = -1;
    double area = 0;
    vector < Point * > p = generate_random_points(n, side, seed, minVal, maxVal);
    for (i = startIndex; i < endIndex; i++) {
        ax = p[i]->getX();
        ay = p[i]->getY();
        for (j = i + 1; j < n; j++) {
            bx = p[j]->getX();
            by = p[j]->getY();
            for (k = j + 1; k < n; k++) {
                cx = p[k]->getX();
                cy = p[k]->getY();
                area = abs((ax * (by - cy) + bx * (cy - ay) + cx * (ay - by)) / 2);
                if (area > largestArea) {
                    largestArea = area;
                    largestIndexes[0] = i;
                    largestIndexes[1] = j;
                    largestIndexes[2] = k;
                    largestIndexes[3] = largestArea;
                }
            }
        }
    }
    cout << "Got largest on process " << rank << " as " << largestArea << endl;
    return largestIndexes;
}

int main(int argc, char **argv) {
    // initialize MPI
    MPI_Init(&argc, &argv);

    int n = atoi(argv[1]);
    int side = atoi(argv[2]);
    double seed = atoi(argv[3]);
    double ax, ay, bx, by, cx, cy, largestArea = -1;
    int largestIndex;
    int numtasks = 0, rank = 0, startIndex, endIndex;
    MPI_Status stat;
    MPI_Datatype columntype;
    // create contiguous derived data type
    MPI_Type_contiguous(4, MPI_DOUBLE, &columntype);
    MPI_Type_commit(&columntype);
    Point *largestA = new Point();
    Point *largestB = new Point();
    Point *largestC = new Point();
    // vector < Point * > p = generate_random_points(n, side, seed, -100, 100);

    // get number of tasks
    MPI_Comm_size(MPI_COMM_WORLD, &numtasks);
    // get my rank
    MPI_Comm_rank(MPI_COMM_WORLD, &rank);
    MPI_Request reqs[2*numtasks];
    double *results[numtasks];
    auto mpi_t1 = MPI_Wtime();
    auto t1 = chrono::high_resolution_clock::now();
    startIndex = floor(n * ((float) rank / (float) numtasks));
    endIndex = floor(n * ((float) (rank + 1) / (float) numtasks));
    results[rank] = find_largest(n, side, seed, 0, 100, startIndex, endIndex, rank);
    // results[rank] = find_largest(p, n, startIndex, endIndex, rank);
    // MPI_Send(results[rank], 4, MPI_DOUBLE, 0, 0, MPI_COMM_WORLD);
    MPI_Isend(results[rank], 4, MPI_DOUBLE, 0, 0, MPI_COMM_WORLD, &reqs[rank]);
    // MPI_Barrier(MPI_COMM_WORLD);
    auto mpi_t2 = MPI_Wtime();
    auto t2 = chrono::high_resolution_clock::now();
    if (rank == 0) {
        double combinedResults[numtasks][numtasks];
        // Compare everyone's largestArea
        vector < Point * > p = generate_random_points(n, side, seed, 0, 100);
        for (int i = 0; i < numtasks; i++) {
            MPI_Recv(combinedResults[i], 4, MPI_DOUBLE, i, 0, MPI_COMM_WORLD, &stat);
            // MPI_Irecv(combinedResults[i], 4, MPI_DOUBLE, i, 0, MPI_COMM_WORLD, &reqs[numtasks+i]);
        }

        for (int i = 0; i < numtasks; i++) {
            if (combinedResults[i][3] > largestArea) {
                largestArea = combinedResults[i][3];
                largestIndex = i;
            }
        }

        auto mpi_duration = mpi_t2 - mpi_t1;
        auto duration = chrono::duration_cast<chrono::milliseconds>(t2 - t1).count();
        cout << "Got Largest amongst all as:: " << largestArea << endl << endl;
        largestA = p[combinedResults[largestIndex][0]];
        largestB = p[combinedResults[largestIndex][1]];
        largestC = p[combinedResults[largestIndex][2]];
        ax = largestA->getX();
        ay = largestA->getY();
        bx = largestB->getX();
        by = largestB->getY();
        cx = largestC->getX();
        cy = largestC->getY();
        cout << combinedResults[largestIndex][0] << ", " << ax << ", " << ay << endl;
        cout << combinedResults[largestIndex][1] << ", " << bx << ", " << by << endl;
        cout << combinedResults[largestIndex][2] << ", " << cx << ", " << cy << endl;
        cout << largestArea << endl;
        cout << "MPI Execution Time (seconds): " << mpi_duration << endl;
        cout << "Execution Time (ms): " << duration << endl;
    }
    // done with MPI
    MPI_Finalize();
    return 0;
}