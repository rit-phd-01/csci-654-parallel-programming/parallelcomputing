#include <math.h>
#include <stdio.h>
#include <bits/stdc++.h>

#define min(a, b) a < b ? a:b
#define max(a, b) a > b ? a:b

#define e 5

typedef unsigned long long int ull;

__device__ int counter = 0;

__global__ void break_rsa(int *c_r, int *n_r, int *m) {
    int temp;
    int n = *n_r;
    int c = *c_r;
    int ctr = 0;
    int index = threadIdx.x + blockIdx.x * blockDim.x;
    int stride = blockDim.x * gridDim.x;
    for (ull i = index; i < n && counter < e; i += stride) {
        temp = (((i * i) % n) * i) % n;
        if (temp == c) {
            ctr = atomicAdd(&counter, 1);
            m[ctr] = (int) i;
        }
    }
}


int main(int argc, char *argv[]) {
    if (!argc) {
        printf("Enter the values c & n");
        return 1;
    }
    // Initiate lazy context creation- With this statement, the CUDA context starts building up, although it doesn't affect the overall execution of time.
    cudaFree(0);
    int c = atoi(argv[1]);
    int n = atoi(argv[2]);

    int m[e];
    for (int i = 0; i < e; i++) {
        m[i] = -1;
    }
    int *d_c, *d_n, *d_m;
    int blocks_per_grid, threads_per_block;
    int max_blocks_per_grid, max_threads_per_block;
    int int_size = sizeof(int *);
    auto t1_main = std::chrono::high_resolution_clock::now();
    // auto t1 = chrono::high_resolution_clock::now();
    cudaMalloc((void **) &d_c, int_size);
    // auto t2 = chrono::high_resolution_clock::now();
    // auto duration = chrono::duration_cast< chrono::microseconds>( t2 - t1 ).count();
    // std::cout << "First allocation time:: " << duration << std::endl;
    // t1 = chrono::high_resolution_clock::now();
    cudaMalloc((void **) &d_n, int_size);
    // t2 = chrono::high_resolution_clock::now();
    // duration = chrono::duration_cast< chrono::microseconds>( t2 - t1 ).count();
    // std::cout << "Second allocation time:: " << duration << std::endl;
    // t1 = chrono::high_resolution_clock::now();
    cudaMalloc((void **) &d_m, int_size * e);
    // t2 = chrono::high_resolution_clock::now();
    // duration = chrono::duration_cast< chrono::microseconds>( t2 - t1 ).count();
    // std::cout << "Third allocation time:: " << duration << std::endl;
    cudaMemcpy(d_c, &c, int_size, cudaMemcpyHostToDevice);
    cudaMemcpy(d_n, &n, int_size, cudaMemcpyHostToDevice);
    cudaMemcpy(d_m, m, int_size * e, cudaMemcpyHostToDevice);

    // Finding block_per_grid and threads_per_block.
    // Ensure tight upper-bound. Could work more towards lowerbound, but using up more free resources doesn't harm
    ull power_of_2 = ceil(log(n) / log(2));
    ull p = pow(2, power_of_2);
    if (n > 256) {
        max_blocks_per_grid = 1024;
        max_threads_per_block = 1024;
        blocks_per_grid = min(max_blocks_per_grid, ceil(p / max_blocks_per_grid));
        threads_per_block = min(max_threads_per_block, ceil(p / blocks_per_grid));
    } else {
        max_blocks_per_grid = 8;
        max_threads_per_block = 32;
        blocks_per_grid = min(max_blocks_per_grid, ceil(p / max_threads_per_block));
        threads_per_block = min(max_threads_per_block, ceil(p / blocks_per_grid));
    }
    printf("Sizes: blocks_per_grid=%d, threads_per_block=%d", blocks_per_grid, threads_per_block);

    break_rsa << < blocks_per_grid, threads_per_block >> > (d_c, d_n, d_m);
    cudaMemcpy(m, d_m, int_size * e, cudaMemcpyDeviceToHost);
    cudaError err = cudaGetLastError();
    auto t2_main = std::chrono::high_resolution_clock::now();
    auto duration_main = std::chrono::duration_cast<std::chrono::milliseconds>(t2_main - t1_main).count();
    std::cout << "\nTotal execution time (ms):: " << duration_main << std::endl;
    if (cudaSuccess != err) {
        printf("Error: !\n");
        printf("%s", cudaGetErrorString(err));
    }

    int no_of_roots = 0;
    for (int i = 0; i < e; i++) {
        if (m[i] >= 0) {
            no_of_roots++;
            printf("\n%d^%d = %d (mod %d)", m[i], e, c, n);
        }
    }
    if (!no_of_roots) {
        printf("\nNo cube roots of %d (mod %d)", c, n);
    }
    printf("\n");
    cudaFree(d_c);
    cudaFree(d_n);
    cudaFree(d_m);
    return 0;
}