#include<bits/stdc++.h>
using namespace std;

class Point {
    private: double x;
    private: double y;
    public: Point() {
        this->x = 0;
        this->y = 0;
    }
    public: Point(double x, double y) {
        this->x = x;
        this->y = y;
    }
    public: double getX() {
        return this->x;
    }
    public: double getY() {
        return this->y;
    }
};

vector<Point*> generate_random_points(int n, int side, double seed, double minVal, double maxVal) {
    // Generate a vector of points
    vector<Point*> p;
    // rd() provides a random seed
    random_device rd;
    default_random_engine generator(rd());
    uniform_real_distribution<double> distribution(minVal, maxVal);
    for(int i=0; i<n; i++) {
        double x = distribution(generator);
        double y = distribution(generator);
        Point* temp = new Point(x, y);
        p.push_back(temp);
    }
    return p;
}

int main(int argc, char **argv){
    cout<<"Starting Largest Rectangle"<<endl;
    int n = atoi(argv[1]);
    int side = atoi(argv[2]);
    double seed = atoi(argv[3]);
    int i, j, k, aIndex, bIndex, cIndex;
    double ax, ay, bx, by, cx, cy, largestArea=-1;
    double area=0;
    Point* largestA = new Point();
    Point* largestB = new Point();
    Point* largestC = new Point();
    vector<Point*> p = generate_random_points(n, side, seed, -100, 100);
    auto t1 = chrono::high_resolution_clock::now();
    for(i=0; i<n; i++) {
        ax = p[i]->getX();
        ay = p[i]->getY();
        for(j=i+1; j<n; j++) {
            bx = p[j]->getX();
            by = p[j]->getY();
            for(k=j+1; k<n; k++) {
                cx = p[k]->getX();
                cy = p[k]->getY();
                area = abs((ax*(by-cy) + bx*(cy-ay) + cx*(ay-by))/2);
                // cout<<i<<", "<<j<<", "<<k<<" = "<<area<<endl;
                if(area > largestArea) {
                    largestArea = area;
                    largestA = p[i];
                    largestB = p[j];
                    largestC = p[k];
                    aIndex = i;
                    bIndex = j;
                    cIndex = k;
                }
            }
        }
    }
    auto t2 = chrono::high_resolution_clock::now();
    auto duration = chrono::duration_cast< chrono::microseconds>( t2 - t1 ).count();
    cout<<aIndex<<", "<<largestA->getX()<<", "<<largestA->getY()<<endl;
    cout<<bIndex<<", "<<largestB->getX()<<", "<<largestB->getY()<<endl;
    cout<<cIndex<<", "<<largestC->getX()<<", "<<largestC->getY()<<endl;
    cout<<largestArea<<endl;
    cout<<"Execution Time (microseconds): "<<duration<<endl;
    return 0;
}