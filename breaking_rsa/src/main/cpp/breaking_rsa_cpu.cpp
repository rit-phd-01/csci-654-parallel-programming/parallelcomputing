#include <bits/stdc++.h>

using namespace std;
typedef unsigned long long int ull;

vector<int> break_rsa(int c, int n) {
    vector<int> m;
    int temp;
    for (ull i = 0; i < n; i++) {
        temp = (((i * i) % n) * i) % n;
        if (temp == c) {
            m.push_back((int)i);
        }
    }
    return m;
}

int main(int argc, char *argv[]) {
    if (!argc) {
        printf("Enter the values c & n");
        return 1;
    }

    int c = atoi(argv[1]);
    int n = atoi(argv[2]);
    int e = 3;
    vector<int> m = break_rsa(c, n);
    if (!m.size()) {
        printf("\nNo cube roots of %d (mod %d)\n", c, n);
        return 0;
    }
    for (int i = 0; i < m.size(); i++) {
        printf("\n%d^%d = %d (mod %d)", m[i], e, c, n);
    }
    printf("\n");
    return 0;
}