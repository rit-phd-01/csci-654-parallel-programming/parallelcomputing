#include <bits/stdc++.h>
#include "sha256.h"


using namespace std;

int proof_of_work(string block_hash, string target_hash) {
    int i;
    string test_string, test_hash;
    for (i = 0; i < INT_MAX; i++) {
        test_string = block_hash + to_string(i);
        test_hash = sha256(sha256(test_string));
        if (test_hash.compare(target_hash) < 0) {
            break;
        }
    }
    return i;
}

int main() {
    cout << "Sequential version of coin mining" << endl;
    string block_hash = sha256("The quick brown fox jumps over the lazy dog!!!!!!");
    cout << "Block hash::: "<< block_hash << endl;
    string target_hash = "0000ab00090ba1a5e38c216ee6790fc1dbc18e8a447b61ec2133ae6cccc93f69"; //"0000092a6893b712892a41e8438e3ff2242a68747105de0395826f60b38d88dc";
    auto t1 = chrono::high_resolution_clock::now();
    int nonce = proof_of_work(block_hash, target_hash);
    auto t2 = chrono::high_resolution_clock::now();
    auto duration = chrono::duration_cast<chrono::microseconds>(t2 - t1).count();
    cout << "Got nonce as:: " << nonce << endl;
    cout << "Execution Time (microseconds): " << duration << endl;
    return 0;
}