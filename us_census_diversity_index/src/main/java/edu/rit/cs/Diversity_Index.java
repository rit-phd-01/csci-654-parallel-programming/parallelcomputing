package edu.rit.cs;

import org.apache.spark.SparkConf;
import org.apache.spark.api.java.JavaSparkContext;
import org.apache.spark.sql.Column;
import org.apache.spark.sql.api.java.UDF6;
import org.apache.spark.sql.*;
import org.apache.spark.sql.types.DataTypes;
import static org.apache.spark.sql.functions.callUDF;


public class Diversity_Index {
    private static final String inputFile = "dataset/census-alldata.csv";
    private static final String outputFolder = "dataset/output";

    private static void compute_diversity_index(SparkSession spark, JavaSparkContext jsc, SQLContext sqlContext, Integer selectedYear) {
        Dataset ds, selectedCols, finalVals;
        sqlContext.udf().register("compute_di", (UDF6<Long, Long, Long, Long, Long, Long, Double>) (WA, BA, IA, AA, NA, TOM) -> {
            Long total = WA + BA + IA + AA + NA + TOM;
            Long[] n = {WA, BA, IA, AA, NA, TOM};
            Long sum = Long.valueOf(0);
            for(int i=0; i<n.length; i++){
                sum += n[i] * (total - n[i]);
            }
            Double ans = Double.valueOf(sum)/Double.valueOf(total*total);
            return ans;
        }, DataTypes.DoubleType);

        ds = spark.read()
                .option("header", "true")
                .option("delimiter", ",")
                .option("inferSchema", "true")
                .csv(inputFile);

        // SUMLEV,STATE,COUNTY,STNAME,CTYNAME,YEAR,AGEGRP,TOT_POP,TOT_MALE,TOT_FEMALE,WA_MALE,WA_FEMALE,BA_MALE,BA_FEMALE,IA_MALE,IA_FEMALE,AA_MALE,AA_FEMALE,NA_MALE,NA_FEMALE,TOM_MALE,TOM_FEMALE,WAC_MALE,WAC_FEMALE,BAC_MALE,BAC_FEMALE,IAC_MALE,IAC_FEMALE,AAC_MALE,AAC_FEMALE,NAC_MALE,NAC_FEMALE,NH_MALE,NH_FEMALE,NHWA_MALE,NHWA_FEMALE,NHBA_MALE,NHBA_FEMALE,NHIA_MALE,NHIA_FEMALE,NHAA_MALE,NHAA_FEMALE,NHNA_MALE,NHNA_FEMALE,NHTOM_MALE,NHTOM_FEMALE,NHWAC_MALE,NHWAC_FEMALE,NHBAC_MALE,NHBAC_FEMALE,NHIAC_MALE,NHIAC_FEMALE,NHAAC_MALE,NHAAC_FEMALE,NHNAC_MALE,NHNAC_FEMALE,H_MALE,H_FEMALE,HWA_MALE,HWA_FEMALE,HBA_MALE,HBA_FEMALE,HIA_MALE,HIA_FEMALE,HAA_MALE,HAA_FEMALE,HNA_MALE,HNA_FEMALE,HTOM_MALE,HTOM_FEMALE,HWAC_MALE,HWAC_FEMALE,HBAC_MALE,HBAC_FEMALE,HIAC_MALE,HIAC_FEMALE,HAAC_MALE,HAAC_FEMALE,HNAC_MALE,HNAC_FEMALE
        Column[] colList = {
                ds.col("STNAME").as("StateName"),
                ds.col("CTYNAME").as("CountyName"),
                ds.col("YEAR").as("Year").cast(DataTypes.LongType),
                ds.col("AGEGRP").as("AgeGroup").cast(DataTypes.LongType),
                ds.col("WA_MALE").plus(ds.col("WA_FEMALE")).as("TotalWA").cast(DataTypes.LongType),
                ds.col("BA_MALE").plus(ds.col("BA_FEMALE")).as("TotalBA").cast(DataTypes.LongType),
                ds.col("IA_MALE").plus(ds.col("IA_FEMALE")).as("TotalIA").cast(DataTypes.LongType),
                ds.col("AA_MALE").plus(ds.col("AA_FEMALE")).as("TotalAA").cast(DataTypes.LongType),
                ds.col("NA_MALE").plus(ds.col("NA_FEMALE")).as("TotalNA").cast(DataTypes.LongType),
                ds.col("TOM_MALE").plus(ds.col("TOM_FEMALE")).as("TotalTOM").cast(DataTypes.LongType),
        };
        selectedCols = ds.filter("AGEGRP = 0").select(colList);

        if (selectedYear != -1) {
            selectedCols = selectedCols.filter("Year = " + selectedYear);
        }

        finalVals = selectedCols.select(
                selectedCols.col("StateName"),
                selectedCols.col("CountyName"),
                callUDF("compute_di",
                        selectedCols.col("TotalWA"),
                        selectedCols.col("TotalBA"),
                        selectedCols.col("TotalIA"),
                        selectedCols.col("TotalAA"),
                        selectedCols.col("TotalNA"),
                        selectedCols.col("TotalTOM")
                ).as("diversity_index")
        ).sort("StateName", "CountyName");
        finalVals.show();
        finalVals
                .coalesce(1)
                .write()
                .format("com.databricks.spark.csv")
                .mode(SaveMode.Overwrite)
                .option("header", true)
                .option("charset", "UTF-8")
                .save(outputFolder);
    }

    public static void main(String[] args) {
        System.out.println("Starting Diversity Index Program!");
        // Create a SparkConf that loads defaults from system properties and the classpath
        SparkConf sparkConf = new SparkConf();
        sparkConf.set("spark.master", "local[8]");
        //Provides the Spark driver application a name for easy identification in the Spark or Yarn UI
        sparkConf.setAppName("Diversity Index");
        // Creating a session to Spark. The session allows the creation of the
        // various data abstractions such as RDDs, DataFrame, and more.
        SparkSession spark = SparkSession.builder().config(sparkConf).getOrCreate();
        // SparkContext sc = new SparkContext(sparkConf);
        SQLContext sqlContext = new SQLContext(spark);
        // Creating spark context which allows the communication with worker nodes
        JavaSparkContext jsc = new JavaSparkContext(spark.sparkContext());
        //  sc, sqlContext,
        compute_diversity_index(spark, jsc, sqlContext, 1);

        // Stop existing spark context
        jsc.close();
        // Stop existing spark session
        spark.close();
    }
}
