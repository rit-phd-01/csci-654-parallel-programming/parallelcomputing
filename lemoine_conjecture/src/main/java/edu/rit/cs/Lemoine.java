package edu.rit.cs;

import java.util.Scanner;

public class Lemoine {
    public static void main( String[] args )
    {
        System.out.println("Starting Lemoine Conjecture");
        System.out.println("Please enter minVal and maxVal ");
        Scanner sc = new Scanner(System.in);
        int minVal = sc.nextInt();
        int maxVal = sc.nextInt();
        if(minVal <= 5 || maxVal < minVal) {
            throw new Error("minVal should be greater than 5 and maxVal should be greater than or equal to minVal!");
        }

        /**
         * Check if minVal, maxVal is odd as we'll be starting iterations based on these numbers
         */
        if(maxVal > minVal) {
            minVal = (minVal % 2 == 1 ? minVal : (minVal + 1));
            maxVal = (maxVal % 2 == 1 ? maxVal : (maxVal + 1));
        }

        long serialTime = LemoineSerial.serialCode(minVal, maxVal);

        long parallelTime = LemoineParallel.parallelCode(minVal, maxVal);

        System.out.println("****************************");
        System.out.println("Speedup:: " + Math.round((double)serialTime/parallelTime -1)*100 + " % ");
    }
}