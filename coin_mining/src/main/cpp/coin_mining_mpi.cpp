#include <bits/stdc++.h>
#include <climits>
#include <mpi.h>
#include "sha256.h"
#define ull unsigned long long

using namespace std;

unsigned long long proof_of_work(string block_hash, string target_hash, ull start_index, ull end_index) {
    unsigned long long int i;
    string test_string, test_hash;
    for (i = start_index; i < end_index; i++) {
        test_string = block_hash + to_string(i);
        test_hash = sha256(sha256(test_string));
        if (test_hash.compare(target_hash) < 0) {
            return i;
        }
    }
    return ULLONG_MAX;
}

int main() {
    MPI_Init(NULL, NULL);
    string block_hash = sha256("The quick brown fox jumps over the lazy dog!!!!!!");
    string target_hash = "0000ab00090ba1a5e38c216ee6790fc1dbc18e8a447b61ec2133ae6cccc93f69";
    int world_rank;
    MPI_Comm_rank(MPI_COMM_WORLD, &world_rank);
    int world_size;
    MPI_Comm_size(MPI_COMM_WORLD, &world_size);
    MPI_Status stat;

    ull results[world_size+1];
    ull chunk_size = ceil((INT_MAX)/world_size);
    ull no_of_tasks = INT_MAX/chunk_size;
    MPI_Request reqs[world_size];
    ull s_index = world_rank*chunk_size;
    ull e_index = (world_rank+1)*chunk_size - 1;
    auto t1 = chrono::high_resolution_clock::now();
    double start_time = MPI_Wtime();
    results[world_rank] = proof_of_work(block_hash, target_hash, s_index, e_index);
    MPI_Isend(&results[world_rank], 1, MPI_UNSIGNED_LONG_LONG, 0, 0, MPI_COMM_WORLD, &reqs[world_rank]);

    int flag = 0;

    if(world_rank == 0) {
        ull combinedResults[no_of_tasks];

        for (int i = 0; i < no_of_tasks; i++) {
            MPI_Recv(&combinedResults[i], 1, MPI_UNSIGNED_LONG_LONG, i, 0, MPI_COMM_WORLD, &stat);
        }

        for (int i = 0; i < no_of_tasks; i++) {
            if (combinedResults[i] != ULLONG_MAX) {
                cout<<"Found nonce as:: "<< combinedResults[i] << endl;
                break;
            }
        }
        double end_time = MPI_Wtime();
        auto t2 = chrono::high_resolution_clock::now();
        auto duration = chrono::duration_cast<chrono::microseconds>(t2 - t1).count();
        cout << "Execution Time (microseconds): " << duration << endl;
        cout << "Execution Time (using MPI): " << end_time-start_time <<endl;
    }

    MPI_Finalize();
    return 0;
}