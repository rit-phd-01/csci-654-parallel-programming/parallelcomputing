#include <bits/stdc++.h>
#include <stdio.h>

using namespace std;

__global__ void helloCUDA() {
    int x = threadIdx.x + blockDim.x * blockIdx.x ;
    int y = threadIdx.y + blockDim.y * blockIdx.y;
    int z = threadIdx.z + blockDim.y * blockIdx.z;
    printf("Hello thread x=%d, threadIdx.x=%d, y=%d, threadIdx.y=%d, z=%d, threadIdx.z=%d, gridDim.x=%d, gridDim.y=%d, blockDim.x=%d, blockIdx.x=%d, blockDim.y=%d\n"
            , x, threadIdx.x, y, threadIdx.y, z, threadIdx.z, gridDim.x, gridDim.y, blockDim.x, blockIdx.x, blockDim.y);
}

int main() {
    dim3 grid(1, 1); // grid = 16 x 16 blocks
    dim3 block(4, 2, 2); // block = 32 x 32 threads
    helloCUDA << < grid, block >> > ();
    cudaDeviceReset();
    return 0;
}