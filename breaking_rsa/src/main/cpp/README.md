## How to execute
### Sequential
```
Compile:    g++ breaking_rsa_cpu.cpp -o breaking_rsa_cpu
Run:        ./breaking_rsa_cpu *c* *n*
```
### Parallel version
```
Compile:    nvcc breaking_rsa_gpu.cu -o breaking_rsa_gpu
Run:        ./breaking_rsa_gpu *c* *n*
```


