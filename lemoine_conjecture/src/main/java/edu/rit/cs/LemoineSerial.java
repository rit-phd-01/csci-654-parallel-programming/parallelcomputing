package edu.rit.cs;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.concurrent.TimeUnit;

/**
 * Class LemoineSerial:
 * Considering the hypothesis of Lemoine's Conjecture,
 * If all odd numbers are of the form n = p + 2 * q,
 * it finds a unique n such that p is largest from all
 * numbers examined such that their p's were the smallest
 *
 * @author avinash
 *
 */
public class LemoineSerial
{
    /**
     * Create an arrayList to hold all the prime numbers to be utilized for this [minVal, maxVal] interval.
     */
    private static ArrayList<Integer> primeNumbers = new ArrayList<>();

    /**
     * Method to check for least <TT>p</TT> for a given <TT>n</TT>. Returns 0 if it fails.
     * @param n
     * @return
     */
    private static int checkLemoine(int n){
        int rem;
        Iterator<Integer> itr = primeNumbers.iterator();
        Integer p = itr.next();
        while(p <= n && itr.hasNext()){
            rem = n-p;
            if(rem%2 == 0 && Prime.isPrime(rem/2)) {
                return p;
            }
            p = itr.next();
        }
        return 0;
    }

    public static long serialCode(int minVal, int maxVal) {
        int p, largestP=0, largestN=0;
        System.out.println("****** STARTING SERIAL EXECUTION ******");
        long startTimeGeneratingPrimes = System.nanoTime();

        /**
         * Generate primes upto maxVal and cache them
         */
        Prime.Iterator itr = new Prime.Iterator();
        Integer prime = new Integer(itr.next());
        while (prime <= maxVal) {
            primeNumbers.add(prime);
            prime = new Integer(itr.next());
        }

        long startTime = System.nanoTime();

        /**
         * Check for all odd numbers till minVal or largestN, whichever is found first.
         */
        for(int i=maxVal; i >= minVal; i-=2) {
            p = checkLemoine(i);
            if(p>0){
                if(p > largestP){
                    largestP = p;
                    largestN = i;
                }
            }
        }

        long endTime = System.nanoTime();


        if(largestN != 0 && largestP != 0) {
            System.out.println(largestN + " = " + largestP + " + 2*" + ((largestN - largestP)/2));
        } else {
            System.out.println("Either the number input is non-prime or the range is insufficient");
        }

        System.out.println("Time Elapsed [ms]:: " + ((endTime-startTime)/1000000));
        System.out.println("Time Elapsed (Include Generating Primes serially) [ms]:: " + ((endTime-startTimeGeneratingPrimes)/1000000));
        return ((endTime - startTime) / 1000000);
    }
}
